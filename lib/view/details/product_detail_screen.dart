import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mvvm/models/barang_lists/barang_main.dart';
import 'package:flutter_mvvm/repository/cart/cart_repo_impl.dart';
import 'package:flutter_mvvm/res/app_context_ext.dart';
import 'package:flutter_mvvm/utils/utils.dart';
import 'package:flutter_mvvm/view/widgets/my_textview_desc_widget.dart';
import 'package:flutter_mvvm/view/widgets/my_textview_widget.dart';

class ProductDetailsScreen extends StatelessWidget {
  static final String id = "product_details";
  final Barang? product;
  final _myRepo = CartRepoImp();
  ProductDetailsScreen(this.product);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(title: Text(product!.title.toString())),
      body: Container(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(context.resources.dimension.mediumMargin),
            child: Column(
              children: [
                Center(
                  child: Image.network(
                    product?.image ?? "",
                    errorBuilder: (context, error, stackTrace) {
                      return new Image.asset('assets/images/img_error.png');
                    },
                    fit: BoxFit.fill,
                    height: context.resources.dimension.imageHeight,
                  ),
                ),
                Card(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      ListTile(
                          title: MyTextView("\$ ${product?.price ?? "NA"}",context.resources.color.colorPrimaryText,context.resources.dimension.bigText,),
                          subtitle: MyTextViewDesc(product?.desc ?? "NA",context.resources.color.colorPrimaryText,context.resources.dimension.mediumText),
                          
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child : TextButton(
                          onPressed: () {
                            Map<String,dynamic> body = {
                              "product_id":product!.id.toString()
                            };

                            _myRepo
                                .addCart(body)
                                .then((value) => ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(content: Text('Add to cart sucess'))))
                                .onError((error, stackTrace) =>ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(content: Text(error.toString()))));
                          }, 
                          child: Text("Add to cart",style: TextStyle(fontSize: 14,fontWeight: FontWeight.bold),)
                        ),
                      )
                      
                    ],
                  ),
                  elevation: context.resources.dimension.lightElevation,
                  margin: EdgeInsets.all(context.resources.dimension.smallMargin),
                  color: context.resources.color.colorWhite,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}