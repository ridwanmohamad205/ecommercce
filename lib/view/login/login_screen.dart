import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_mvvm/utils/change_page_login.dart';
import 'package:flutter_mvvm/view/widgets/my_chip.dart';
import 'package:flutter_mvvm/view/widgets/my_login_card.dart';
import 'package:flutter_mvvm/view/widgets/my_textview_widget.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  static final String id = "login_screen";
  @override
  _LoginScreenState createState() => _LoginScreenState();

}

class _LoginScreenState extends State<LoginScreen> {

  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();
  TextEditingController email = new TextEditingController();
  TextEditingController password = new TextEditingController();
  List<FocusNode> _focusNodes = [FocusNode(), FocusNode()];
  bool _obscureText = true;
  void initState() {
    super.initState();
    ChangePageLoginNotifier notif = ChangePageLoginNotifier();
  }
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue.shade900,
      body: Center(
        child: Container(
          padding: EdgeInsets.only(bottom: 32, left: 32, right: 32),
          child: Column(
            children: [
              Spacer(),
              MyTextView("MY ECOMMERCE", Colors.white, 24),
              MyLoginCard(_formKey, _focusNodes, email, password, _obscureText, _isLoading, _toggle),
              Spacer(),
              MyTextView("Ridwan Dev", Colors.white, 14),
            ],
          ),
        ),
      )
    );
  }
  
}