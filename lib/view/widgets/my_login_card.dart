import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_mvvm/data/local/user_secure_storage.dart';
import 'package:flutter_mvvm/repository/login/login_repo_impl.dart';
import 'package:flutter_mvvm/utils/change_page_login.dart';
import 'package:flutter_mvvm/view/home/home.dart';
import 'package:flutter_mvvm/view_model/login/login_view_model.dart';
class MyLoginCard extends StatelessWidget {
  final _myRepo = LoginRepoImpl();
  final _formKey;
  final _focusNodes;
  final email;
  final password;
  final _obscureText;
  final _isLoading;
  final _toggle;
  MyLoginCard(this._formKey,this._focusNodes,this.email,this.password, this._obscureText,this._isLoading,this._toggle);
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.only(top: 24),
      child: Container(
        width: 300,
        padding: EdgeInsets.all(18),
        child: Column(
          children: <Widget>[
            Container(
              // margin: EdgeInsets.symmetric(vertical: 10),
              child: Form(
                  key: _formKey,
                  child: Column(
                    // crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          TextFormField(
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please input your email';
                              }
                              return null;
                            },
                            controller: email,
                            enabled: true,
                            focusNode: _focusNodes[0],
                            style: TextStyle(color: Colors.teal.shade400),
                            keyboardType: TextInputType.emailAddress,
                            decoration: InputDecoration(
                              // contentPadding: EdgeInsets.symmetric(vertical: 8),
                              helperText: '',
                              enabledBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey.shade400)),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.teal.shade400)),
                              hintText: "Email",
                              hintStyle: TextStyle(
                                  color: Colors.grey[400], fontSize: 14),
                            ),
                          ),
                        ],
                      ),
                      // Padding(
                      //   padding: const EdgeInsets.only(top: 8),
                      //   child:
                      Column(
                        children: <Widget>[
                          TextFormField(
                            validator: (value) {
                              if (value == null || value.isEmpty) {
                                return 'Please input your password';
                              }
                              return null;
                            },
                            controller: password,
                            obscureText: _obscureText,
                            enabled: true,
                            focusNode: _focusNodes[1],
                            style: TextStyle(color: Colors.teal.shade400),
                            decoration: InputDecoration(
                              helperText: '',
                              enabledBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: Colors.grey.shade400)),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.teal.shade400)),
                              // suffixIconConstraints:
                              // BoxConstraints(minWidth: 23, maxHeight: 20),
                              suffixIcon: Padding(
                                padding: const EdgeInsets.only(left: 4),
                                child: IconButton(
                                  icon: Icon(
                                      _obscureText
                                          ? Icons.remove_red_eye_rounded
                                          : Icons.remove_red_eye_outlined,
                                      color: _focusNodes[1].hasFocus
                                          ? Colors.teal.shade400
                                          : Colors.grey[400]),
                                  onPressed: _toggle,
                                ),
                              ),
                              hintText: "Password",
                              hintStyle: TextStyle(
                                  color: Colors.grey[400], fontSize: 14),
                            ),
                          )
                        ],
                      ),
                      // ),
                      Padding(
                        padding: EdgeInsets.only(top: 32, bottom: 18),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              primary: Colors.teal.shade400,
                              fixedSize: Size.fromWidth(double.maxFinite),
                              padding: EdgeInsets.symmetric(vertical: 18)),
                          onPressed: _isLoading ? null : () {
                            // Validate returns true if the form is valid, or false otherwise.
                            if (_formKey.currentState!.validate()) {
                              // If the form is valid, display a snackbar. In the real world,
                              // you'd often call a server or save the information in a database.
                              ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(content: Text('Processing Data')));
                              Map<String,dynamic> body = {
                                "email":email.text,
                                "password":password.text
                              };

                              void loggedIn(token){
                                UserSecureStorage pref = UserSecureStorage();
                                pref.setToken(token);
                                Navigator.pushNamed(context, Home.id);
                              }

                              _myRepo
                                  .getLogin(body)
                                  .then((value) => loggedIn(value?.token))
                                  .onError((error, stackTrace) =>ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(content: Text(error.toString()))));
                            }
                          },
                          child: Text(_isLoading ? 'Processing...' : 'LOGIN'),
                        ),
                      ),
                      Text(
                        'Lupa Password',
                        style: TextStyle(
                            color: Colors.teal,
                            fontSize: 12,
                            fontStyle: FontStyle.italic),
                      ),
                    ],
                  )),
            )
          ],
        ),
      ),
    );
  }
}