import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyTextViewDesc extends StatelessWidget {
  final label;
  final Color color;
  final double fontSize;
  MyTextViewDesc(this.label,this.color,this.fontSize);
  @override
  Widget build(BuildContext context) {
    return Text(
        label,
        textAlign: TextAlign.start,
        style: TextStyle(
          color: color,
          fontSize: fontSize
        ),
      );
  }
}