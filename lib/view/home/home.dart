import 'package:custom_navigation_bar/custom_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mvvm/data/local/user_secure_storage.dart';
import 'package:flutter_mvvm/models/barang_lists/barang_main.dart';
import 'package:flutter_mvvm/models/cart_list/cart_main.dart';
import 'package:flutter_mvvm/models/profile/profile_main.dart';
import 'package:flutter_mvvm/view/details/product_detail_screen.dart';
import 'package:flutter_mvvm/view/login/login_screen.dart';
import 'package:flutter_mvvm/view_model/home/home_view_model.dart';
import 'package:flutter_mvvm/view/widgets/my_textview_widget.dart';
import 'package:flutter_mvvm/view/widgets/my_error_widget.dart';
import 'package:flutter_mvvm/view/widgets/loading_widget.dart';
import 'package:flutter_mvvm/res/app_context_ext.dart';
import 'package:flutter_mvvm/utils/utils.dart';
import 'package:provider/provider.dart';
import 'package:flutter_mvvm/data/remote/responses/status.dart';

class Home extends StatefulWidget {

  static final String id = "home";

  @override
  _HomeState createState() => _HomeState();

}

class _HomeState extends State<Home> {
  final Home2VM viewModel = Home2VM();
  int _currentIndex = 0;

  @override
  void initState() {
    viewModel.fetchBarang();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Center(child: MyTextView(context.resources.strings.homeScreen, context.resources.color.colorWhite, context.resources.dimension.bigText)),
      //   backgroundColor: Theme.of(context).primaryColor,
      // ),
      body: ChangeNotifierProvider<Home2VM>(
        create: (BuildContext context) => viewModel,
        child: Consumer<Home2VM>(builder: (context, viewModel, _) {
          if (this._currentIndex == 0) {
            switch (viewModel.barangMain.status) {
              case Status.LOADING:
                print("RIDWAN :: LOADING");
                return LoadingWidget();
              case Status.ERROR:
                print("RIDWAN :: ERROR");
                return MyErrorWidget(viewModel.barangMain.message ?? "NA");
              case Status.COMPLETED:
                print("RIDWAN :: COMPLETED");
                return _selectedMenu();
                
              default:
            }
          }

          if (this._currentIndex == 1) {
            switch (viewModel.cartMain.status) {
              case Status.LOADING:
                print("RIDWAN :: LOADING");
                return LoadingWidget();
              case Status.ERROR:
                print("RIDWAN :: ERROR");
                if (viewModel.cartMain.message!.contains("Unauthorised")) {
                  return Container(
                    child: TextButton(onPressed: () => {
                        Navigator.pushNamed(context, LoginScreen.id)
                      }, 
                      child: Align(alignment: Alignment.center, child:Text("LOGIN"))
                      )
                    );
                }
                return MyErrorWidget(viewModel.cartMain.message ?? "NA");
              case Status.COMPLETED:
                print("RIDWAN :: COMPLETED");
                return _selectedMenu();
                
              default:
            }
          }
          if (this._currentIndex == 2) {
            switch (viewModel.profileMain.status) {
              case Status.LOADING:
                print("RIDWAN :: LOADING");
                return LoadingWidget();
              case Status.ERROR:
                print("RIDWAN :: ERROR");
                if (viewModel.profileMain.message!.contains("Unauthorised")) {
                  return Container(
                    child: TextButton(onPressed: () => {
                        Navigator.pushNamed(context, LoginScreen.id)
                      }, 
                      child: Align(alignment: Alignment.center, child:Text("LOGIN"))
                      )
                    );
                }
                return MyErrorWidget(viewModel.profileMain.message ?? "NA");
              case Status.COMPLETED:
                print("RIDWAN :: COMPLETED");
                return _selectedMenu();
                
              default:
            }
          }

          
          return Container();
        }),
      ),
      bottomNavigationBar: CustomNavigationBar(
        iconSize: 30.0,
        selectedColor: Colors.white,
        strokeColor: Colors.white,
        unSelectedColor: Color(0xff6c788a),
        backgroundColor: Color(0xff040307),
        items: [
          CustomNavigationBarItem(
            icon: Icon(Icons.home),
          ),
          CustomNavigationBarItem(
            icon: Icon(Icons.shopping_cart),
          ),
          CustomNavigationBarItem(
            icon: Icon(Icons.account_circle),
          ),
        ],
        currentIndex: _currentIndex,
        onTap: (index) {
          
          setState(() {
            // _currentIndex = index;
            switch (index) {
              case 0:
                _currentIndex = index;
                viewModel.fetchBarang();
                break;
              case 1:
                _currentIndex = index;
                viewModel.fetchCart();
                break;
              case 2:
                _currentIndex = index;
                viewModel.fetchUserDetail();
                break;
              default:
                _currentIndex = index;
                break;
            }
          });
        },
      ),
    );
  }

  Widget _selectedMenu() {
    switch (_currentIndex) {
      case 0:
        // viewModel.fetchBarang();
        return _getBarangListView(viewModel.barangMain.data?.barang);
      case 1:
        // viewModel.fetchCart();
        return _getCartListView(viewModel.cartMain.data?.cart,viewModel.cartMain.data?.total.toString());
      case 2:
        return _getProfileView(viewModel.profileMain.data?.profile);
      default:
        return _getBarangListView(viewModel.barangMain.data?.barang);
    }
  }

  Widget _getProfileView(Profile? profile){
    return SingleChildScrollView(
      physics: ScrollPhysics(),
      child: Column(
        children: [
          Card(
            child: Padding(padding: EdgeInsets.symmetric(vertical: 8,horizontal: 16),
              child:Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text("Nama",style: TextStyle(fontSize: 8),),
                  Text(profile!.name.toString(),style: TextStyle(fontSize: 16),),

                  Text("Email",style: TextStyle(fontSize: 8),),
                  Text(profile.email.toString(),style: TextStyle(fontSize: 16),),
                ],
              )
            ),
          ),
          TextButton(
            onPressed: () {
            _logOut();
          }, 
          child: Text("LOGOUT",style: TextStyle(color: Colors.red[600]),))
        ],
      )
    );
  }

  _logOut() async {
    UserSecureStorage pref = UserSecureStorage();
    (await pref.getPreferences()).clear();
    Navigator.pushNamed(context, Home.id);
  }

  Widget _getCartListView(List<Cart>? cartList,String? total) {
    return SingleChildScrollView(
      physics: ScrollPhysics(),
      child: Column(
        children: [
          ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            itemCount: cartList?.length,
            itemBuilder: (context, position) {
              return _getCartListItem(cartList![position]);
            },
            shrinkWrap: true,
          ),
          Card(
            child: Padding(
              child: Row(
                children: [
                  Text("Total Belanja"),
                  Spacer(),
                  Text("\$ ${total}")
                ],
              ),
              padding: EdgeInsets.symmetric(vertical: 16,horizontal: 24),
            )
            
          )
        ]
      )
    );
  }

  Widget _getCartListItem(Cart item) {
    return Card(
      child: Padding(
        padding: EdgeInsets.all(8),
        child: ListTile(
          leading: ClipRRect(
            child: Image.network(
              item.image ?? "",
              errorBuilder: (context, error, stackTrace) {
                return new Image.asset('assets/images/img_error.png');
              },
              fit: BoxFit.fill,
              width: context.resources.dimension.listImageSize,
              height: context.resources.dimension.listImageSize,
            ),
            borderRadius: BorderRadius.circular(context.resources.dimension.imageBorderRadius),
          ),
          title: MyTextView(item.title ?? "NA",context.resources.color.colorPrimaryText,context.resources.dimension.mediumText),
          subtitle: MyTextView(item.desc??"NA",context.resources.color.colorSecondaryText,context.resources.dimension.smallText),
          trailing: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              MyTextView(item.qty.toString(),context.resources.color.colorBlack,context.resources.dimension.bigText),
              SizedBox(width: context.resources.dimension.verySmallMargin),
              MyTextView("\$ ${item.price.toString()}",context.resources.color.colorBlack,context.resources.dimension.mediumText),
            ],
          )
        ),
      ),
      
      elevation: context.resources.dimension.lightElevation,
    );
  }

  Widget _getBarangListView(List<Barang>? barangList) {
    return Container(
      padding: EdgeInsets.symmetric(vertical:10,horizontal: 8),
      child: GridView.builder(
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
        ),
        itemCount: barangList?.length,
        itemBuilder: (context, position) {
          return _getBarangListItem(barangList![position]);
        }
      ),
    );
  }

  Widget _getBarangListItem(Barang item) {
    return GestureDetector(
      onTap: () => _sendDataToBarangDetailScreen(context, item),
      child: Card(
        child: Container(
          padding: EdgeInsets.all(24),
          child: Column(
            children: [
              Image.network(
                item.image ?? "",
                errorBuilder: (context, error, stackTrace) {
                  return new Image.asset('assets/images/img_error.png');
                },
                fit: BoxFit.fill,
                width: context.resources.dimension.listImageSize,
                height: context.resources.dimension.listImageSize,
              ),
              // Container(
              //   alignment: Alignment.centerLeft,
              //   margin: EdgeInsets.only(top: 14),
              //   child: MyTextView(item.title ?? "NA",context.resources.color.colorPrimaryText,context.resources.dimension.smallText),
              // ),
              // Container(
              //   alignment: Alignment.centerLeft,
              //   margin: EdgeInsets.only(top: 14),
              //   child: MyTextView("\$ ${item.price ?? "NA"}",context.resources.color.colorPrimaryText,context.resources.dimension.mediumText)
              // ),
              
            ],
          ),
        ),
        
        // child: ListTile(
        //   leading: ClipRRect(
        //     child: Image.network(
        //       item.image ?? "",
        //       errorBuilder: (context, error, stackTrace) {
        //         return new Image.asset('assets/images/img_error.png');
        //       },
        //       fit: BoxFit.fill,
        //       width: context.resources.dimension.listImageSize,
        //       height: context.resources.dimension.listImageSize,
        //     ),
        //     borderRadius: BorderRadius.circular(context.resources.dimension.imageBorderRadius),
        //   ),
        //   title: MyTextView(item.title ?? "NA",context.resources.color.colorPrimaryText,context.resources.dimension.bigText),
        //   subtitle: MyTextView(item.desc??"NA",context.resources.color.colorSecondaryText,context.resources.dimension.mediumText),
        //   onTap: () {
        //     _sendDataToBarangDetailScreen(context, item);
        //   },
        // ),
        elevation: context.resources.dimension.lightElevation,
      )
    );
  }

  void _sendDataToBarangDetailScreen(BuildContext context, Barang item) {
    Navigator.pushNamed(context, ProductDetailsScreen.id,arguments: item);
  }
}