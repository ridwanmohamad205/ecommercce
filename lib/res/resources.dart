import 'package:flutter_mvvm/res/colors/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_mvvm/res/dimentions/app_dimentions.dart';
import 'package:flutter_mvvm/res/strings/strings.dart';
import 'package:flutter_mvvm/res/strings/english_string.dart';

class Resources {
  BuildContext _context;

  Resources(this._context);

  Strings get strings {
    // It could be from the user preferences or even from the current locale
    Locale locale = Localizations.localeOf(_context);
    switch (locale.languageCode) {
      // case 'fr':
        // return FrenchStrings();
      default:
        return EnglishStrings();
    }
  }

  AppColors get color {
    return AppColors();
  }

  AppDimension get dimension {
    return AppDimension();
  }

  static Resources of(BuildContext context){
    return Resources(context);
  }
}