import 'package:flutter/cupertino.dart';
import 'package:flutter_mvvm/res/resources.dart';

extension AppContext on BuildContext {
  Resources get resources => Resources.of(this);
}