import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:flutter_mvvm/data/local/user_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_mvvm/data/remote/app_exception.dart';
import 'package:flutter_mvvm/data/remote/networks/base_api_services.dart';

class NetworkApiService extends BaseApiService {

  @override
  Future getResponse(String url) async {
    dynamic responseJson;
    try {
      UserSecureStorage pref = UserSecureStorage();
      
      String token = "";
      await pref.getToken().then((value) => token = value!).onError((error, stackTrace) => token = "");
      Map<String,String> header = {
        "Accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded",
      };
      if (token.isNotEmpty) {
        header = {
          "Accept": "application/json",
          "Content-Type": "application/x-www-form-urlencoded",
          "Authorization": "Bearer $token",
        };
      }
      final response = await http.get(Uri.parse(baseUrl + url),
      headers: header);
      log(response.statusCode.toString());
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }
  
  @override
  Future postResponse(String url, Map<String,dynamic> body) async {
    dynamic responseJson;
    try {
      UserSecureStorage pref = UserSecureStorage();
      
      String token = "";
      await pref.getToken().then((value) => token = value!).onError((error, stackTrace) => token = "");
      Map<String,String> header = {
        "Accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded",
      };
      if (token.isNotEmpty) {
        header = {
          "Accept": "application/json",
          "Content-Type": "application/x-www-form-urlencoded",
          "Authorization": "Bearer $token",
        };
      }

      final response = await http.post(
        Uri.parse(baseUrl + url), 
        headers: header,
        body: body
      );

      log(response.body);
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet Connection');
    }
    return responseJson;
  }

  dynamic returnResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        dynamic responseJson = jsonDecode(response.body);
        return responseJson;
      case 400:
        throw BadRequestException(response.toString());
      case 401:
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 404:
        throw UnauthorisedException(response.body.toString());
      case 500:
      default:
        throw FetchDataException(
            'Error occured while communication with server' +
                ' with status code : ${response.statusCode}');
    }
  }
}