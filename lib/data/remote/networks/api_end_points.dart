class ApiEndPoints {
  final String getMovies = "movies_list";
  final String getBarang = "getProducts";
  final String getCart = "getCarts";
  final String addCart = "addCarts";
  final String getUserDetail = "user/detail";
  final String logout = "logout";
  final String login = "login";
}