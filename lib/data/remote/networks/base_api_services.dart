abstract class BaseApiService {

  final String baseUrl = "https://ridwandev.web.id:9001/api/";

  Future<dynamic> getResponse(String url);
  Future<dynamic> postResponse(String url, Map<String,dynamic> body);

}