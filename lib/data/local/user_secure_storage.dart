import 'package:shared_preferences/shared_preferences.dart';

class UserSecureStorage {
  Future<SharedPreferences> getPreferences() async =>
      await SharedPreferences.getInstance();

  static const _keyToken = 'token';

  setToken(String token) async => (await getPreferences()).setString(_keyToken, token);

  Future<String?> getToken() async => (await getPreferences()).getString(_keyToken);
}