import 'dart:convert';

CartMain CartMainFromJson(String str) => CartMain.fromJson(json.decode(str));

String CartMainToJson(CartMain data) => json.encode(data.toJson());

class CartMain {
  CartMain({
    this.cart,
    this.total
  });

  List<Cart>? cart;
  double? total;

  factory CartMain.fromJson(Map<String, dynamic> json) => CartMain(
    cart: json["cart"] == null ? null : List<Cart>.from(json["cart"].map((x) => Cart.fromJson(x))),
    total: json["total"] == null ? null : json["total"],
  );

  Map<String, dynamic> toJson() => {
    "cart": cart == null ? null : List<dynamic>.from(cart!.map((x) => x.toJson())),
    "total": total == null ? null : total,
  };
}

class Cart {
  Cart({
    this.id,
    this.product_id,
    this.title,
    this.desc,
    this.price,
    this.image,
    this.qty
  });

  int? id;
  int? product_id;
  String? title;
  String? desc;
  String? price;
  String? image;
  int? qty;

  factory Cart.fromJson(Map<String, dynamic> json) => Cart(
    id: json["id"] == null ? null : json["id"],
    product_id: json["product_id"] == null ? null : json["product_id"],
    title: json["title"] == null ? null : json["title"],
    desc: json["desc"] == null ? null : json["desc"],
    price: json["price"] == null ? null : json["price"],
    image: json["image"] == null ? null : json["image"],
    qty: json["jumlah"] == null ? null : json["jumlah"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "product_id": product_id == null ? null : product_id,
    "title": title == null ? null : title,
    "desc": desc == null ? null : desc,
    "price": price == null ? null : price,
    "image": image == null ? null : image,
    "jumlah": qty == null ? null : qty,
  };
}

// enum OriginalTitle { EMPTY, ANNIHILATION, A_WRINKLE_IN_TIME, THE_LEISURE_SEEKER, CE_QUI_NOUS_LIE }

/*final originalTitleValues = EnumValues({
  "Annihilation": OriginalTitle.ANNIHILATION,
  "A Wrinkle in Time": OriginalTitle.A_WRINKLE_IN_TIME,
  "Ce qui nous lie": OriginalTitle.CE_QUI_NOUS_LIE,
  "": OriginalTitle.EMPTY,
  "The Leisure Seeker": OriginalTitle.THE_LEISURE_SEEKER
});*/

class EnumValues<T> {
  late Map<String, T> map;
  late Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}