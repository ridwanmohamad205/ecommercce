import 'dart:convert';

ProfileMain ProfileMainFromJson(String str) => ProfileMain.fromJson(json.decode(str));

String profileMainToJson(ProfileMain data) => json.encode(data.toJson());

class ProfileMain {
  ProfileMain({
    this.profile
  });

  Profile? profile;

  factory ProfileMain.fromJson(Map<String, dynamic> json) => ProfileMain(
    profile: json["data_user"] == null ? null :  Profile.fromJson(json["data_user"]),
  );

  Map<String, dynamic> toJson() => {
    "profile": profile == null ? null : profile,
  };
}

class Profile {
  Profile({
    this.id,
    this.name,
    this.email,
  });

  int? id;
  String? name;
  String? email;

  factory Profile.fromJson(Map<String, dynamic> json) => Profile(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    email: json["email"] == null ? null : json["email"]
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "email": email == null ? null : email,
  };
}

class EnumValues<T> {
  late Map<String, T> map;
  late Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}