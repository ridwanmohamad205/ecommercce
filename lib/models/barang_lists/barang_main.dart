import 'dart:convert';

BarangMain barangMainFromJson(String str) => BarangMain.fromJson(json.decode(str));

String barangMainToJson(BarangMain data) => json.encode(data.toJson());

class BarangMain {
  BarangMain({
    this.barang,
  });

  List<Barang>? barang;

  factory BarangMain.fromJson(Map<String, dynamic> json) => BarangMain(
    barang: json["products"] == null ? null : List<Barang>.from(json["products"].map((x) => Barang.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "products": barang == null ? null : List<dynamic>.from(barang!.map((x) => x.toJson())),
  };
}

class Barang {
  Barang({
    this.id,
    this.title,
    this.desc,
    this.price,
    this.image
  });

  int? id;
  String? title;
  String? desc;
  String? price;
  String? image;

  factory Barang.fromJson(Map<String, dynamic> json) => Barang(
    id: json["id"] == null ? null : json["id"],
    title: json["title"] == null ? null : json["title"],
    desc: json["desc"] == null ? null : json["desc"],
    price: json["price"] == null ? null : json["price"],
    image: json["image"] == null ? null : json["image"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "title": title == null ? null : title,
    "desc": desc == null ? null : desc,
    "price": price == null ? null : price,
    "image": image == null ? null : image,
  };
}

// enum OriginalTitle { EMPTY, ANNIHILATION, A_WRINKLE_IN_TIME, THE_LEISURE_SEEKER, CE_QUI_NOUS_LIE }

/*final originalTitleValues = EnumValues({
  "Annihilation": OriginalTitle.ANNIHILATION,
  "A Wrinkle in Time": OriginalTitle.A_WRINKLE_IN_TIME,
  "Ce qui nous lie": OriginalTitle.CE_QUI_NOUS_LIE,
  "": OriginalTitle.EMPTY,
  "The Leisure Seeker": OriginalTitle.THE_LEISURE_SEEKER
});*/

class EnumValues<T> {
  late Map<String, T> map;
  late Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}