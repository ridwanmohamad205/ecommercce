import 'dart:convert';

LoginMain loginMainFromJson(String str) => LoginMain.fromJson(json.decode(str));

String loginMainToJson(LoginMain data) => json.encode(data.toJson());

class LoginMain {
  LoginMain({
    this.login,
  });

  List<Login>? login;

  factory LoginMain.fromJson(Map<String, dynamic> json) => LoginMain(
    login: json["login"] == null ? null : List<Login>.from(json["login"].map((x) => Login.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "login": login == null ? null : List<dynamic>.from(login!.map((x) => x.toJson())),
  };
}

class Login {
  Login({
    this.token,
    // this.userData,
    this.status
  });

  String? token;
  int? status;

  factory Login.fromJson(Map<String, dynamic> json) => Login(
    token: json["auth_token"] == null ? null : json["auth_token"],
    // userData: json["data_user"] == null ? null : List<String>.from(json["data_user"].map((x) => x)),
    status: json["status_code"] == null ? null : json["status_code"]
  );

  Map<String, dynamic> toJson() => {
    "auth_token": token == null ? null : token,
    // "userData": userData == null ? null : List<dynamic>.from(userData!.map((x) => x)),
    "status_code":status == null ? null : status
  };
}

// enum OriginalTitle { EMPTY, ANNIHILATION, A_WRINKLE_IN_TIME, THE_LEISURE_SEEKER, CE_QUI_NOUS_LIE }

/*final originalTitleValues = EnumValues({
  "Annihilation": OriginalTitle.ANNIHILATION,
  "A Wrinkle in Time": OriginalTitle.A_WRINKLE_IN_TIME,
  "Ce qui nous lie": OriginalTitle.CE_QUI_NOUS_LIE,
  "": OriginalTitle.EMPTY,
  "The Leisure Seeker": OriginalTitle.THE_LEISURE_SEEKER
});*/

class EnumValues<T> {
  late Map<String, T> map;
  late Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}