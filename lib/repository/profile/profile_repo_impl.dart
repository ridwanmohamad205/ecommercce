import 'dart:convert';
import 'dart:developer';
import 'package:flutter_mvvm/data/remote/networks/base_api_services.dart';
import 'package:flutter_mvvm/data/remote/networks/network_api_services.dart';
import 'package:flutter_mvvm/data/remote/networks/api_end_points.dart';
import 'package:flutter_mvvm/models/profile/profile_main.dart';
import 'package:flutter_mvvm/repository/profile/profile_repo.dart';

class ProfileRepoImpl implements ProfileRepo{

  BaseApiService _apiService = NetworkApiService();

  @override
  Future<ProfileMain?> getProfile() async {
    // try {
      dynamic response = await _apiService.getResponse(
          ApiEndPoints().getUserDetail);
      log(response.toString());
      final jsonData = ProfileMain.fromJson(response);
      log(jsonData.toString());
      return jsonData;
    // } catch (e) {
    //   print("RIDWAN-E $e}");
    //   throw e;
    // }
  }

}