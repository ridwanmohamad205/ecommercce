import 'dart:convert';
import 'package:flutter_mvvm/data/remote/networks/base_api_services.dart';
import 'package:flutter_mvvm/data/remote/networks/network_api_services.dart';
import 'package:flutter_mvvm/data/remote/networks/api_end_points.dart';
import 'package:flutter_mvvm/models/barang_lists/barang_main.dart';
import 'package:flutter_mvvm/models/cart_list/cart_main.dart';
import 'package:flutter_mvvm/repository/barang/barang_repo.dart';

class BarangRepoImp implements BarangRepo{

  BaseApiService _apiService = NetworkApiService();

  @override
  Future<BarangMain?> getBarangList() async {
    try {
      dynamic response = await _apiService.getResponse(
          ApiEndPoints().getBarang);
      final jsonData = BarangMain.fromJson(response);
      return jsonData;
    } catch (e) {
      throw e;
      print("RIDWAN-E $e}");
    }
  }

}