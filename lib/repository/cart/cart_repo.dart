import 'package:flutter_mvvm/models/cart_list/cart_main.dart';

class CartRepo{
  Future<CartMain?> getCartList() async {}
  Future<Cart?> addCart(Map<String,dynamic> body) async {}
}