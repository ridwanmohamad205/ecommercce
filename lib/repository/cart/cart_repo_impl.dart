import 'dart:convert';
import 'dart:developer';
import 'package:flutter_mvvm/data/remote/networks/base_api_services.dart';
import 'package:flutter_mvvm/data/remote/networks/network_api_services.dart';
import 'package:flutter_mvvm/data/remote/networks/api_end_points.dart';
import 'package:flutter_mvvm/models/cart_list/cart_main.dart';
import 'package:flutter_mvvm/repository/cart/cart_repo.dart';

class CartRepoImp implements CartRepo{

  BaseApiService _apiService = NetworkApiService();

  @override
  Future<CartMain?> getCartList() async {
    try {
      dynamic response = await _apiService.getResponse(
          ApiEndPoints().getCart);
      final jsonData = CartMain.fromJson(response);
      return jsonData;
    } catch (e) {
      print("RIDWAN-E $e}");
      throw e;
    }
  }

  @override
  Future<Cart?> addCart(Map<String,dynamic> body) async {
    try {
      dynamic response = await _apiService.postResponse(
          ApiEndPoints().addCart,body);
      final jsonData = Cart.fromJson(response);
      return jsonData;
    } catch (e) {
      print("RIDWAN-E $e}");
      throw e;
    }
  }
}