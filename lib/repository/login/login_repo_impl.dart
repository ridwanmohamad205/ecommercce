import 'dart:convert';
import 'dart:developer';
import 'package:flutter_mvvm/data/remote/networks/base_api_services.dart';
import 'package:flutter_mvvm/data/remote/networks/network_api_services.dart';
import 'package:flutter_mvvm/data/remote/networks/api_end_points.dart';
import 'package:flutter_mvvm/models/login/login_main.dart';
import 'package:flutter_mvvm/repository/login/login_repo.dart';

class LoginRepoImpl implements LoginRepo{

  BaseApiService _apiService = NetworkApiService();

  @override
  Future<Login?> getLogin(Map<String,dynamic> body) async {
    try {
      dynamic response = await _apiService.postResponse(
          ApiEndPoints().login,body);

      log(response.toString());
      final jsonData = Login.fromJson(response);
      return jsonData;
    } catch (e) {
      print("RIDWAN-E $e}");
      throw e;
    }
  }

}