import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter_mvvm/data/remote/responses/api_responses.dart';
import 'package:flutter_mvvm/models/login/login_main.dart';
import 'package:flutter_mvvm/repository/login/login_repo_impl.dart';

class LoginVM extends ChangeNotifier {
  final _myRepo = LoginRepoImpl();

  ApiResponse<LoginMain> loginMain = ApiResponse.loading();

  void _setLoginMain(ApiResponse<LoginMain> response) {
    loginMain = response;
    notifyListeners();
  }

  Future<void> postLogin(Map<String,dynamic> body) async {
    log(body.toString());
    // _setLoginMain(ApiResponse.loading());
    // _myRepo
    //     .getLogin(body)
    //     .then((value) => _setLoginMain(ApiResponse.completed(value)))
    //     .onError((error, stackTrace) =>
    //     _setLoginMain(ApiResponse.error(error.toString())));
  }
}