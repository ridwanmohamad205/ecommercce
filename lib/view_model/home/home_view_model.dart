import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_mvvm/data/remote/responses/api_responses.dart';
import 'package:flutter_mvvm/models/barang_lists/barang_main.dart';
import 'package:flutter_mvvm/models/cart_list/cart_main.dart';
import 'package:flutter_mvvm/models/profile/profile_main.dart';
import 'package:flutter_mvvm/repository/barang/barang_repo_impl.dart';
import 'package:flutter_mvvm/repository/cart/cart_repo_impl.dart';
import 'package:flutter_mvvm/repository/profile/profile_repo_impl.dart';

class Home2VM extends ChangeNotifier {
  final _myRepo = BarangRepoImp();
  final _myCartRepo = CartRepoImp();
  final _myProfileRepo = ProfileRepoImpl();

  ApiResponse<BarangMain> barangMain = ApiResponse.loading();
  ApiResponse<CartMain> cartMain = ApiResponse.loading();
  ApiResponse<ProfileMain> profileMain = ApiResponse.loading();

  void _setBarangMain(ApiResponse<BarangMain> response) {
    barangMain = response;
    notifyListeners();
  }

  void _setCartMain(ApiResponse<CartMain> response) {
    cartMain = response;
    notifyListeners();
  }

  void _setProfileMain(ApiResponse<ProfileMain> response) {
    profileMain = response;
    notifyListeners();
  }

  Future<void> fetchBarang() async {
    _setBarangMain(ApiResponse.loading());
    _myRepo
        .getBarangList()
        .then((value) => _setBarangMain(ApiResponse.completed(value)))
        .onError((error, stackTrace) => _setBarangMain(ApiResponse.error(error.toString())));
  }

  Future<void> fetchCart() async {
    _setCartMain(ApiResponse.loading());
    _myCartRepo
        .getCartList()
        .then((value) => _setCartMain(ApiResponse.completed(value)))
        .onError((error, stackTrace) => _setCartMain(ApiResponse.error(error.toString())));
  }
  Future<void> fetchUserDetail() async {
    _setProfileMain(ApiResponse.loading());
    _myProfileRepo
        .getProfile()
        .then((value) => _setProfileMain(ApiResponse.completed(value)))
        .onError((error, stackTrace) => _setProfileMain(ApiResponse.error(error.toString())));
  }
}