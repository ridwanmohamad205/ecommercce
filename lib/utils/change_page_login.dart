import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter_mvvm/data/local/user_secure_storage.dart';

class ChangePageLoginNotifier extends ChangeNotifier {
  String _token = "";
  String get token => _token;


  ChangePageLoginNotifier() {
    _token = "";
    _loadFromPreferences();
  }

  _loadFromPreferences() async {
    UserSecureStorage _user_secure_storage = UserSecureStorage();
    _token = await _user_secure_storage.getToken() ?? "";
    log(_token);
    notifyListeners();
  }
}