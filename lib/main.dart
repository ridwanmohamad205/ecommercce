import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_mvvm/data/local/user_secure_storage.dart';
import 'package:flutter_mvvm/models/barang_lists/barang_main.dart';
import 'package:flutter_mvvm/utils/change_page_login.dart';
import 'package:flutter_mvvm/view/details/product_detail_screen.dart';
import 'package:flutter_mvvm/view/home/home.dart';
import 'package:flutter_mvvm/res/app_context_ext.dart';
import 'package:flutter_mvvm/view/login/login_screen.dart';
import 'package:provider/provider.dart';
String token = "";

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  UserSecureStorage secureStorage = UserSecureStorage();
  token = await secureStorage.getToken() ?? "";
  print('initScreen ${token}');
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
      return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          colorScheme: ColorScheme.fromSwatch(primarySwatch: context.resources.color.colorPrimary).copyWith(secondary: context.resources.color.colorAccent),
        ),
        initialRoute: Home.id,
        routes: {
          Home.id:(context) => Home(),
          LoginScreen.id: (context) => LoginScreen(),
          ProductDetailsScreen.id: (context) => ProductDetailsScreen(ModalRoute.of(context)!.settings.arguments as Barang),
        },
      );
  }
}